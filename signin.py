import signin_helper

#dodac hashowanie hasel sha256

def login():
    email = input("Podaj email: ")
    password = input("Podaj haslo: ")
    users = signin_helper.login(email, password)
    if len(users) == 1:
        print(users)
        print("Zalogowano sie.")
    elif len(users) > 1:
        print("Blad bazy danych.")
    else:
        print("Wprowadzono nieprawidlowy login lub haslo.")
    menu()

def register():
    email = input("Podaj email: ")
    password1 = input("Podaj haslo: ")
    password2= input("Powtorz haslo: ")
    name = input("Podaj imie: ")
    surname = input("Podaj nazwisko: ")
    age = int(input("Podaj wiek: "))
    phoneNO = input("Podaj nr telefonu: ")
    gender = input("Podaj plec: ")

    if password1 != password2:
        print("Podane hasla nie zgadzaja sie.")
        menu()
    if age < 0:
        print("Podano nieprawidlowy wiek.")
        menu()
    if not (gender[0].lower() == "m" or gender[0].lower() == "k"):
        print("Podano nieprawidlowa plec.")
        menu()
    
    signin_helper.register(email, password1, name, surname, age, phoneNO, gender)
    print("Utworzono uzytkownika o nazwie", email)
    menu()

def users_list():
    users = signin_helper.users_list()
    print(users)
    menu()

def change_password():
    email = input("Podaj email: ")
    old_password = input("Podaj stare hasło: ")
    users = signin_helper.login(email, old_password)
    if len(users) == 1:
        print(users)
        print("Zalogowano sie.")
    elif len(users) > 1:
        print("Blad bazy danych.")
        menu()
    else:
        print("Wprowadzono nieprawidlowy login lub haslo.")
        menu()
    new_password1 = input("Podaj nowe haslo: ")
    new_password2= input("Powtorz nowe haslo: ")
    if new_password1 != new_password2:
        print("Podane hasla nie zgadzaja sie.")
        menu()
    else:
        signin_helper.change_password(new_password1, email, old_password)
        print("Twoje hasło zostało zmienione.")
    menu()

def delete_account():
    email = input("Podaj email: ")
    password = input("Podaj hasło: ")
    users = signin_helper.login(email, password)
    if len(users) == 1:
        signin_helper.delete_account(email, password)
        print("Twoje konto zostało usunięte.")
        menu()
    elif len(users) > 1:
        print("Blad bazy danych.")
        menu()
    else:
        print("Wprowadzono nieprawidlowy login lub hasło.")
        menu()
    
def menu():
    print("""
    ===========MENU===========
    1. Logowanie
    2. Rejestracja
    3. Lista użytkowników
    4. Zmien haslo
    5. Usun konto
    6. Wyjscie""")

    choise = int(input("\nTwoj wybor: "))

    if (choise == 1):
        login()
    if (choise == 2):
        register()
    if (choise == 3):
        users_list()
    if (choise == 4):
        change_password()
    if (choise == 5):
        delete_account()
    if (choise == 6):
        quit()

menu()
