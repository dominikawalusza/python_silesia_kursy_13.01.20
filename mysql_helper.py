import mysql.connector
def get_all_employees():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="test"
    )

    mycursor = mydb.cursor()
    mycursor.execute("SELECT * FROM employee")
    myresult = mycursor.fetchall()
    return myresult

def get_one_employee(id):
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="test"
    )

    mycursor = mydb.cursor()

    # mycursor.execute("SELECT * FROM employee where id = " + str(id) )
    sql = f"SELECT * FROM employee where id = {id}"
    mycursor.execute(sql)
    return mycursor.fetchall()

def create_employee(first_name, last_name, salary):
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="test"
    )

    mycursor = mydb.cursor()
    sql = "INSERT INTO employee (first_name, last_name, salary) VALUES (%s, %s, %s)"
    values = (first_name, last_name, salary)
    mycursor.execute(sql, values)
    mydb.commit()

