import mysql.connector


def login(email, password):
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="test"
    )

    mycursor = mydb.cursor()
    sql = "SELECT * FROM user where email like %s and password like %s"
    values = (email, password)
    mycursor.execute(sql, values)

    return mycursor.fetchall()

def register(email, password, name, surname, age, phoneNO, gender):
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="test"
    )

    mycursor = mydb.cursor()
    sql = "INSERT INTO user (email, password, name, surname, age, phoneNO, gender) VALUES (%s, %s, %s, %s, %s, %s, %s)"
    values = (email, password, name, surname, str(age), str(phoneNO), gender)
    mycursor.execute(sql, values)
    mydb.commit()

def users_list():
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="test"
    )

    mycursor = mydb.cursor()
    mycursor.execute("SELECT * FROM user")
    myresult = mycursor.fetchall()
    return myresult

def change_password(new_password1, email, old_password):
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="test"
    )

    mycursor = mydb.cursor()
    sql = ("UPDATE user set password = %s where email = %s and password = %s")
    values = (new_password1, email, old_password)
    mycursor.execute(sql, values)
    mydb.commit()


def delete_account(email, password):
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="test"
    )

    mycursor = mydb.cursor()
    sql = ("DELETE FROM user WHERE email = %s and password = %s")
    values = (email, password)
    mycursor.execute(sql, values)
    mydb.commit()

def end():
    pass

    

